import { Controller, Get, UsePipes, Body, Post, Param } from '@nestjs/common';
import { AppService } from './app.service';
import { AppDto } from './app.dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post()
  getHello(@Body() body: AppDto) {
    console.log(body)
  }
}
