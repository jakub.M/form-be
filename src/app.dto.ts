import { IsEmail } from 'class-validator';
import { Form } from '../decorators/form.decoratro';
import { string } from 'yup';

export class AppDto {
  @Form(string().required())
  email: string;
}
