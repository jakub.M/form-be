import { string, StringSchema, object, ObjectSchema } from 'yup';
import { Type, BadRequestException } from '@nestjs/common';

export const Form = (data?: StringSchema<string>) => (target: any, propertyKey: string) => FormValidation.registerProp(target, propertyKey, data);

interface Prop {
  name: string;
  validator: StringSchema<string>;
}

interface FormError {
  field: string;
  description: string;
}

const prepareError = (err) => err.inner.map(({ path, message }) => ({
  field: path,
  description: message,
}))

export class FormValidation {
  private static schemaMap: Map<any, Prop[]> = new Map();

  static registerProp(target: any, propertyKey: string, data?: StringSchema<string>): void {
    const keyName = target.constructor.name
    let keys = this.schemaMap.get(keyName);
    if (!keys) {
      keys = [];
      this.schemaMap.set(keyName, keys);
    }
    keys.push({ name: propertyKey, validator: data ?? string() });
  }

  static async valid(value: any, metatype: Type<any>) {
    const schema = this.getSchema(metatype);
    if (!schema) {
      return value;
    }
    await schema.validate(value, { abortEarly: false })
    .catch(err => {
      throw new BadRequestException(prepareError(err));
    })
    return this.transform(value,metatype);
  }

  static getSchema(metatype: Type<any>): ObjectSchema<object> {
    const schemaProps = this.schemaMap.get(metatype.name);
    if (!schemaProps) {
      return undefined;
    }
    return object().shape(schemaProps.reduce((res, prop) => ({ ...res, [prop.name]: prop.validator }), {}))
  }

  static transform(value: any, metatype: Type<any>) {
    const schemaProps = this.schemaMap.get(metatype.name);
    return schemaProps
      .reduce((res, { name }) => ({ ...res, [name]: value[name] }) ,{})
  }
}
